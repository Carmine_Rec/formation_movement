#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>


float leader_x, leader_y, leader_head;
float pos_x, pos_y, heading;

void odometryCallback_lead(const nav_msgs::Odometry::ConstPtr& msg)
{
leader_x=msg->pose.pose.position.x+1;
leader_y=msg->pose.pose.position.y;
float ang_z=msg->pose.pose.orientation.z;
float ang_w=msg->pose.pose.orientation.w;
leader_head =2*atan2 (ang_z, ang_w);
}


void odometryCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
pos_x=msg->pose.pose.position.x;
pos_y=msg->pose.pose.position.y+1;
float ang_z=msg->pose.pose.orientation.z;
float ang_w=msg->pose.pose.orientation.w;
heading =2*atan2 (ang_z, ang_w);
}



int main(int argc, char **argv)
{
ros::init(argc, argv, "keep_form");
ros::NodeHandle nh;
ros::Rate loop_rate (100);

ros::Subscriber leader_pos = nh.subscribe("/robot1/odom", 1, odometryCallback_lead);
ros::Subscriber odom = nh.subscribe("/robot2/odom", 1, odometryCallback);
ros::Publisher vel_pub = nh.advertise<geometry_msgs::Twist>("/robot2/mobile_base/commands/velocity", 1);


geometry_msgs::Twist vel;


while(ros::ok())
{


vel_pub.publish(vel);
ros::spinOnce();
loop_rate.sleep();
}

return 0;

}
